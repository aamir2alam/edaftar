package com.example.aamiralam.edaftar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.aamiralam.edaftar.Models.Customer;
import com.example.aamiralam.edaftar.Models.Item;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;

public class Home extends AppCompatActivity {


    private static final String BACK_STACK_ROOT_TAG = "home_fragment";
    BottomNavigationView bottomNavigation;
    FirebaseAuth mAuth;
    private HomeFragment homeFragment;
    private DashboardFragment dashboardFragment;
    private DailySalesFragment dailySalesFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        if(savedInstanceState == null){
            homeFragment = new HomeFragment();
            dashboardFragment = new DashboardFragment();
            dailySalesFragment = new DailySalesFragment();
        }
        // initialize bottom navigation object
        bottomNavigation = (BottomNavigationView) findViewById(R.id.navigation);
        bottomNavigation.setOnNavigationItemSelectedListener(navListener);

        //initialize toolbar
        // add defailt fragment on startup
//        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
//                new HomeFragment(), BACK_STACK_ROOT_TAG).commit();
       // HomeFragment home = new HomeFragment();
        //loadFragment(home, BACK_STACK_ROOT_TAG);
        //HomeFragment homeFragment = new HomeFragment();

        //loading starting fragment as home fragment

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if(homeFragment.isAdded()){
            fragmentTransaction.show(homeFragment);
        }else{
            fragmentTransaction.add(R.id.fragment_container, homeFragment,"home");
        }
        fragmentTransaction.commit();
    }



    public void startNewActivity(Customer customer){
        Intent intent = new Intent(this, CustomerThreadActivity.class);
        intent.putExtra("customer",customer);
        startActivity(intent);
    }





    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

                    switch (item.getItemId()){
                        case R.id.navigation_home:
                            if (homeFragment.isAdded()) { // if the fragment is already in container
                                ft.show(homeFragment);
                            } else { // fragment needs to be added to frame container
                                ft.add(R.id.fragment_container, homeFragment, "home");
                            }
                            // Hide fragment Dashboard
                            if (dashboardFragment.isAdded()) { ft.hide(dashboardFragment); }
                            // Hide fragment dailysales
                            if (dailySalesFragment.isAdded()) { ft.hide(dailySalesFragment); }
                            // Commit changes
                            ft.commit();
                            //HomeFragment homeFragment = new HomeFragment();
                            //checkAndLoadFragment(homeFragment , BACK_STACK_ROOT_TAG);
                            break;
                        case R.id.navigation_dashboard:
                            if (dashboardFragment.isAdded()) { // if the fragment is already in container
                                ft.show(dashboardFragment);
                            } else { // fragment needs to be added to frame container
                                ft.add(R.id.fragment_container, dashboardFragment, "dashboard");
                            }
                            // Hide fragment Dashboard
                            if (homeFragment.isAdded()) { ft.hide(homeFragment); }
                            // Hide fragment dailysales
                            if (dailySalesFragment.isAdded()) { ft.hide(dailySalesFragment); }
                            // Commit changes
                            ft.commit();
                            //DashboardFragment dashboardFragment = new DashboardFragment();
                            //checkAndLoadFragment(dashboardFragment,"dashboard_fragment");
                            break;
                        case R.id.navigation_daily_sales:
                            if (dailySalesFragment.isAdded()) { // if the fragment is already in container
                                ft.show(dailySalesFragment);
                            } else { // fragment needs to be added to frame container
                                ft.add(R.id.fragment_container, dailySalesFragment, "dailySales");
                            }
                            // Hide fragment Dashboard
                            if (homeFragment.isAdded()) { ft.hide(homeFragment); }
                            // Hide fragment dailysales
                            if (dashboardFragment.isAdded()) { ft.hide(dashboardFragment); }
                            // Commit changes
                            ft.commit();
                            //DailySalesFragment dailySalesFragment = new DailySalesFragment();
                            //checkAndLoadFragment(dailySalesFragment,"daily_ales_fragment");
                            break;

                    }

                    return true;
                }
            };

    private void checkAndLoadFragment(Fragment fragment,String tag){

        Log.d("fragment ","fragment is "+tag);

        Fragment savedFragment = getSupportFragmentManager().findFragmentByTag(tag);
        if(savedFragment != null){
            loadFragment(savedFragment , tag);
            Log.d("load","loading fragment from stack");
        }else{
            loadFragment(fragment, tag);
        }

    }

    private void displayFragment(Fragment fragment){

    }

    private void loadFragment(Fragment fragment, String tag){
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container , fragment,tag)
                .addToBackStack(null)
                .commit();

    }

    @Override
    public void onBackPressed() {
        onBackExitApp();
//        Fragment rootFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
//        Log.d("fragment","=  "+rootFragment.toString());
//        Fragment homeFragment = getSupportFragmentManager().findFragmentByTag(BACK_STACK_ROOT_TAG);
//        if( rootFragment instanceof DashboardFragment || rootFragment instanceof  DailySalesFragment ){
//            //HomeFragment homeFragment = new HomeFragment();
//           //navListener.onNavigationItemSelected(homeFragment.)
//            checkAndLoadFragment(homeFragment,BACK_STACK_ROOT_TAG);
//            bottomNavigation.setSelectedItemId(R.id.home_fragment);
//        }else{
//            Log.d("fragment","back pressed");
//            //super.onBackPressed();
//
//        }

    }

    boolean doubleBackToExitPressedOnce = false;

    public void onBackExitApp() {
        if (doubleBackToExitPressedOnce) {
            finish();
            System.exit(0);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

//    override fun onBackPressed() {
//        val currentFragment = this.supportFragmentManager.findFragmentById(R.id.frameContainer)
//        val rootFragment = this.supportFragmentManager.findFragmentById(R.id.rootLayout)
//        if (currentFragment !is EventsFragment && rootFragment !is EventDetailsFragment) {
//            loadFragment(EventsFragment())
//            navigation.selectedItemId = navigation_events
//        } else {
//            super.onBackPressed()
//        }
//    }




}
