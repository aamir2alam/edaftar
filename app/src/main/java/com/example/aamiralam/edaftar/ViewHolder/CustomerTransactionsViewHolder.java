package com.example.aamiralam.edaftar.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.aamiralam.edaftar.R;

public class CustomerTransactionsViewHolder extends RecyclerView.ViewHolder {

    public TextView textViewItems, textViewDate, textViewTotal ;
    public ImageView imageView;

    // @desc used with firebaseAdapter to render the view
    public CustomerTransactionsViewHolder(View itemView) {
        super(itemView);

        textViewItems = itemView.findViewById(R.id.textViewItems);
        textViewDate = itemView.findViewById(R.id.textViewDate);
        textViewTotal = itemView.findViewById(R.id.textViewTotal);
        imageView = itemView.findViewById(R.id.transaction_icon);

    }
}
