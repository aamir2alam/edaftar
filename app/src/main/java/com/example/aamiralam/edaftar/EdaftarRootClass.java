package com.example.aamiralam.edaftar;

import com.google.firebase.database.FirebaseDatabase;

public class EdaftarRootClass  extends android.app.Application{
    @Override
    public void onCreate() {
        super.onCreate();
        // Enable disk persistence
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }
}
