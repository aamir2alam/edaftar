package com.example.aamiralam.edaftar;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aamiralam.edaftar.Dialogs.AddItemsDialog;
import com.example.aamiralam.edaftar.Models.Customer;
import com.example.aamiralam.edaftar.Models.Item;
import com.example.aamiralam.edaftar.Models.ShoppingCart;
import com.example.aamiralam.edaftar.Models.Transaction;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class AddItemsActivity extends AppCompatActivity  implements AddItemsDialog.AddItemsListener{

    Button add_transaction;
    FloatingActionButton add_to_card;
    TextView textViewGrandTotal;
   // EditText editText_item , editText_amount;
    private ArrayList<ShoppingCart> purchased_items = new ArrayList<>();
    private String total_purchase_amount =  "0";

    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_items2);

        Intent intent = getIntent();
        final Customer customer = (Customer)intent.getSerializableExtra("customer");

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference("cust_transactions").child(mAuth.getUid()).child(customer.get_id()).child("transactions");
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        add_to_card = findViewById(R.id.button_Add_to_cart);
        add_transaction  = findViewById(R.id.button_transaction);
        textViewGrandTotal = findViewById(R.id.header_total);

        ListView listView = findViewById(R.id.items_added);

        ItemsAdapter itemsAdapter = new ItemsAdapter();
        listView.setAdapter(itemsAdapter);


        add_to_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Item item = new Item(editText_item.getText().toString(),editText_amount.getText().toString(),"12");
//                items.add(item);
//                editText_item.setText("");
//                editText_amount.setText("");
                openDialog(v);
            }
        });

        add_transaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("items",purchased_items);
                setResult(RESULT_OK, intent);
                //create a transaction
                Calendar c = Calendar.getInstance();
//                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
//                String formatted_date_string = df.format(c.getTime());
                long time_stamp = c.getTime().getTime();
                Transaction transaction = new Transaction("purchase",purchased_items,total_purchase_amount,time_stamp);
                recordTransaction(transaction);
                Log.d("record","items "+purchased_items.size());
            }
        });
    }

    public  void openDialog(View v){
        AddItemsDialog addItemsDialog = new AddItemsDialog();
        addItemsDialog.show(getSupportFragmentManager(),"Add item dialog");
    }

    public void recordTransaction(Transaction transaction){
       if(purchased_items.size() == 0 || purchased_items == null){
           Toast.makeText(this,"Please add atleast one irem for transaction",Toast.LENGTH_SHORT).show();
           return;
       }

        String push_key = mDatabase.push().getKey();
        mDatabase.push().setValue(transaction);
        this.onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void applyText(Item item, String number_of_items) {
        Log.d("item",item.getName()+" number of items  " +number_of_items);

        String total_price = String.valueOf( Integer.parseInt(item.getPrice())*Integer.parseInt(number_of_items) );
        ShoppingCart shoppingCart = new ShoppingCart(item, number_of_items, total_price);
        total_purchase_amount =  String.valueOf(Integer.parseInt(total_purchase_amount) + Integer.parseInt(total_price));

        textViewGrandTotal.setText(total_purchase_amount);
        purchased_items.add(shoppingCart);

        Log.d("item","num "+ total_purchase_amount);
    }


    class ItemsAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return purchased_items.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            convertView = getLayoutInflater().inflate(R.layout.items_list,null);

            TextView textViewItemName = convertView.findViewById(R.id.textView__item_name);
            TextView textViewItemAmount = convertView.findViewById(R.id.textView__item_amount);
            TextView textViewTotal = convertView.findViewById(R.id.textViewTotal);

            textViewItemName.setText(purchased_items.get(position).getCart_item().getName());
            textViewItemAmount.setText(purchased_items.get(position).getNumber_of_items());
            textViewTotal.setText(purchased_items.get(position).getTotal_price());


            return convertView;
        }
    }

}
