package com.example.aamiralam.edaftar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aamiralam.edaftar.Models.Customer;
import com.example.aamiralam.edaftar.Models.Transaction;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AddMoneyActivity extends AppCompatActivity {

    EditText editText_money;
    Button add_money;
    TextView textViewMoney, textViewHeading;

    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    private String current_total;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_money);

        editText_money = findViewById(R.id.editTextMoney);
        add_money = findViewById(R.id.addMoney);
        textViewMoney = findViewById(R.id.Total);
        textViewHeading = findViewById(R.id.textView);

        Intent intent = getIntent();
        current_total = intent.getStringExtra("current_total");
        final Customer customer = (Customer)intent.getSerializableExtra("customer");
        Log.d("data","total "+ current_total );

        textViewMoney.setText(current_total);
        textViewHeading.setText("Remaining amount");

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference("cust_transactions")
                        .child(mAuth.getUid()).child(customer.get_id()).child("transactions");

        add_money.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String entered_amount = editText_money.getText().toString();

                if(entered_amount == null || entered_amount.equals("")){
                    Toast.makeText(getApplicationContext(),"Invalid amount",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(Integer.parseInt(entered_amount) > Integer.parseInt(current_total)){
                    Toast.makeText(getApplicationContext(),"Greater than the max payable amount",Toast.LENGTH_SHORT).show();
                    return;
                }

                Calendar c = Calendar.getInstance();
//                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
//                String formatted_date_string = df.format(c.getTime());
                  long time_stamp = c.getTime().getTime();
                Transaction transaction = new Transaction("payment",entered_amount,time_stamp);

                recordTransaction(transaction);
            }


        });
    }


    public void recordTransaction(Transaction transaction){
        Log.d("transaction",transaction.toString());
        String push_key = mDatabase.push().getKey();
        mDatabase.push().setValue(transaction);
        this.onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
