package com.example.aamiralam.edaftar;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.aamiralam.edaftar.Models.Vendors;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import android.Manifest;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


 // login activity @root
public class MainActivity extends AppCompatActivity {

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    EditText editTextPhone;
    Button button;
    private ProgressDialog progress;
  //  ProgressBar progressBarBar;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private String phone_number;
    private Boolean isCodeSend = true; // by default button response to send code action

    String codeSent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

      //  FirebaseDatabase.getInstance().setPersistenceEnabled(true);

        mAuth = FirebaseAuth.getInstance();
        Intent activityIntent;

        // go straight to main if a token is stored
        Log.d("auth","UId =="+ mAuth.getUid());
        if (mAuth.getUid() != null) {
            activityIntent = new Intent(this, Home.class);
            startActivity(activityIntent);
            finish();

            Log.d("user","uid "+mAuth.getUid());
        }

        //create database instance object
        mDatabase = FirebaseDatabase.getInstance().getReference("registered_vendors");

        editTextPhone = findViewById(R.id.editTextPhone);
        button = (Button)findViewById(R.id.buttonGetVerificationCode);
      //  progressBarBar = (ProgressBar)findViewById(R.id.pBar);

       // button.setText("Login to Edaftar");
        editTextPhone.setHint("Enter Phone");




        progress=new ProgressDialog(this);  // initate progress bar object to use in  the app

        findViewById(R.id.buttonGetVerificationCode).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isCodeSend == true) {
                    // check entered phone number validity
                    String phone = editTextPhone.getText().toString();

                    if(phone.isEmpty()){
                        editTextPhone.setError("Phone number is required");
                        editTextPhone.requestFocus();
                        return;
                    }

                    if(phone.length() > 13 || phone.length() < 5){
                        editTextPhone.setError("Enter valid phone");
                        editTextPhone.requestFocus();
                        return;
                    }

                    if(phone.length() < 10 ){
                        editTextPhone.setError("Please enter a valid phone");
                        editTextPhone.requestFocus();
                        return;
                    }

                    progress.setMessage("Sending OTP..");
                    progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progress.setCanceledOnTouchOutside(false);
                    progress.show();

                    sendVerificationCode();

                }else if(isCodeSend == false){
                    String otp = editTextPhone.getText().toString();

                    if(otp.isEmpty() || otp.length() < 6){
                        Toast.makeText(getApplicationContext(),"Please enter a valid OTP",Toast.LENGTH_SHORT).show();
                        return;
                    }

                    progress.setMessage("Verifying OTP");
                    progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progress.setCanceledOnTouchOutside(false);
                    progress.show();
                    verifySignInCode();
                }else{
                    Toast.makeText(getApplicationContext(),
                            "Please wait until the code is being sent", Toast.LENGTH_LONG).show();
                }
            }
        });
        //grant permissions for sms
        if (checkAndRequestPermissions()) {
            // carry on the normal flow, as the case of  permissions  granted.
        }

    }

// code for automatic sms varification
    private BroadcastReceiver receiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equalsIgnoreCase("otp")) {
            final String message = intent.getStringExtra("message");
            String otp = message.replaceAll("[^0-9]","");
            editTextPhone.setText(otp);
           // TextView tv = (TextView) findViewById(R.id.txtview);
            //tv.setText(message);
        }
    }
};

    private  boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS);
        int receiveSMS = ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS);
        int readSMS = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (receiveSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECEIVE_MMS);
        }
        if (readSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_SMS);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.SEND_SMS);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

     @Override
     protected void onStart() {
         super.onStart();
     }

     @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }




    private void verifySignInCode(){
        String code = editTextPhone.getText().toString();

        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(codeSent, code);
        signInWithPhoneAuthCredential(credential);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {


                        if (task.isSuccessful()) {
                            //here you can open new activity
                            progress.dismiss();

                            //Log.d("auth","uid "+mAuth.getUid()+ "user "+mAuth.getCurrentUser());
                             //this method save user info if he is login for the first time
                            CheckAndSaveUser(phone_number);
                            // navigate to home page after login
                            Intent myIntent = new Intent(MainActivity.this, Home.class);
                            startActivity(myIntent);
                            finish();

                        } else {
                            progress.dismiss();
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                Toast.makeText(getApplicationContext(),
                                        "Incorrect Verification Code ", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                });

    }

    public void CheckAndSaveUser(final String phone_number){

        Query query = mDatabase.orderByChild("phone").equalTo(phone_number);

        query.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                    if(dataSnapshot.getValue() != null){
                        Log.d("save","already registered");
                        Toast.makeText(getApplicationContext(),"Welcome Back",Toast.LENGTH_SHORT).show();
                    }else {
                        SaveUserEntry(phone_number);
                        Toast.makeText(getApplicationContext(),
                                "Welcome to Edaftar", Toast.LENGTH_LONG).show();
                    }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getApplicationContext(),"Network error",Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void SaveUserEntry(String phone){
        Vendors vendor = new Vendors(phone , "","","");

        try {
            mDatabase.child("registered_vendors").child(mAuth.getUid()).setValue(vendor);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private void sendVerificationCode(){

        String phone = editTextPhone.getText().toString();
        phone_number = "+91"+phone;  // store phone number to save in firebase


        button.setText("Sending OTP");
        editTextPhone.setHint("Waiting for OTP");
        isCodeSend = null;
        //progressBarBar.setVisibility(View.VISIBLE);
      //  progress=new ProgressDialog(this);




        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phone_number,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks
    }

    //start a spinner while sending otp



    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            progress.dismiss();
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
             progress.dismiss();
             isCodeSend = true;
             editTextPhone.setHint("Enter Phone");
             button.setText("Login to Edaftar");
             Log.d("code",e.toString());
         //   progressBarBar.setVisibility(View.GONE);
            Toast.makeText(getApplicationContext(),
                    "Unable to sent code", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);

            isCodeSend = false;  // button response to submit code
            codeSent = s;
            button.setText("Confirm OTP");
            editTextPhone.setText(null);
            editTextPhone.setHint("Enter OTP");
         //   progressBarBar.setVisibility(View.GONE);
            progress.dismiss();
            Toast.makeText(getApplicationContext(),
                    "OTP has been sent", Toast.LENGTH_LONG).show();
        }
    };
}
