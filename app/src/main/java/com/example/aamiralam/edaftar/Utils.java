package com.example.aamiralam.edaftar;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

public class Utils extends DialogFragment {
    //Supply keys for the Bundle
    public static final String TITLE_ID = "title";
    public static final String MESSAGE_ID = "message";

    private ProgressDialog progress;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //Get supplied title and message body.
        Bundle messages = getArguments();

        Context context = getActivity();
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setPositiveButton("OK", null);

        if (messages != null) {
            //Add the arguments. Supply a default in case the wrong key was used, or only one was set.
            builder.setTitle(messages.getString(TITLE_ID, "Sorry"));
            builder.setMessage(messages.getString(MESSAGE_ID, "There was an error."));
        } else {
            //Supply default text if no arguments were set.
            builder.setTitle("Sorry");
            builder.setMessage("There was an error.");
        }

        AlertDialog dialog = builder.create();
        return dialog;
    }



    public void showProgress() {
        progress=new ProgressDialog(getContext());  // initate progress bar object to use in  the app
        progress.setMessage("Please wait..");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setCanceledOnTouchOutside(false);
        progress.show();

    }

    public void dissmissProgress() {
        progress=new ProgressDialog(getContext());  // initate progress bar object to use in  the app
        progress.setMessage("Please wait..");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setCanceledOnTouchOutside(false);
        progress.show();
    }
}
