package com.example.aamiralam.edaftar.Dialogs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.example.aamiralam.edaftar.Models.Item;
import com.example.aamiralam.edaftar.R;

import java.util.ArrayList;
import java.util.List;



public class AutoCompleteItemsAdapter  extends ArrayAdapter<Item> {

    private List<Item> Itemslist;
    private List<Item> tempList;
    private List<Item> suggestionList;


    public AutoCompleteItemsAdapter(@NonNull Context context, int resource, @NonNull List<Item> objects) {
        super(context, resource, objects);

        Itemslist = objects;
        tempList = new ArrayList<>(Itemslist);

        suggestionList = new ArrayList<>();

    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //return super.getView(position, convertView, parent);
        if (convertView == null)
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.items_name_auto_complete, parent, false);

        TextView textView = convertView.findViewById(R.id.textView_auto_complete_list_items);

        Item item = Itemslist.get(position);

        textView.setText(item.getName() + "  " + item.getPrice());

        return convertView;

    }

    @NonNull
    @Override
    public Filter getFilter() {
        return itemFilter;
    }

    Filter itemFilter = new Filter() {

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            Item item = (Item) resultValue;

            return item.getName() + "  Rs. " + item.getPrice();

        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                suggestionList.clear();
                constraint = constraint.toString().trim().toLowerCase();

                for (Item item : tempList) {

                    if (item.getName().toLowerCase().contains(constraint)) {
                        suggestionList.add(item);
                    }
                }

                filterResults.count = suggestionList.size();
                filterResults.values = suggestionList;

            }

            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<Item> uList = (ArrayList<Item>) results.values;

            if (results != null && results.count > 0) {
                clear();
                for (Item u : uList) {
                    add(u);
                    notifyDataSetChanged();
                }
            }
        }

    };


}
