package com.example.aamiralam.edaftar.Models;


import java.io.Serializable;

public class Customer implements Serializable {
    private String name;
    private String phone;
    private String _id;
    private long date;

    public Customer(){
    }


    public Customer(String name, String phone, String _id, long date) {
        this.name = name;
        this.phone = phone;
        this._id = _id;
        this.date = date;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }
}
