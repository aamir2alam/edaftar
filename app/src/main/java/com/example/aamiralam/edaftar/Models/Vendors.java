package com.example.aamiralam.edaftar.Models;

public class Vendors {

    private String phone;
    private String name;
    private String shop_number;
    private String address;

    public Vendors(String phone, String name, String shop_number, String address) {
        this.phone = phone;
        this.name = name;
        this.shop_number = shop_number;
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShop_number() {
        return shop_number;
    }

    public void setShop_number(String shop_number) {
        this.shop_number = shop_number;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


}
