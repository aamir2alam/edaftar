package com.example.aamiralam.edaftar.Firebase;

import android.util.Log;
import android.widget.Adapter;

import com.example.aamiralam.edaftar.Models.Customer;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;

public class FirebaseHelper {

    DatabaseReference db;
    Boolean saved;

    public ArrayList<Customer> customers = new ArrayList<Customer>();

    /*
    *  Pass Database reference  to the node where data needs to be stored
    * */

    public FirebaseHelper(DatabaseReference db) {
        this.db = db;


    }

    //Write if not null
    public Boolean Save(Customer customer){

        if(customer == null ){
            saved = false;
        }else{

            try {
                db.push().setValue(customer);
                saved = true;
            } catch (DatabaseException e) {
                e.printStackTrace();
                saved = false;
            }

        }

        return saved;
    }




//    //Implement fetch data and fill arrayList
//    private void FetchData(DataSnapshot dataSnapshot){
//        customers.clear();
//
//        for(DataSnapshot ds : dataSnapshot.getChildren()){
//              Customer customer = new Customer();
//          //  Customer customer = new Customer(ds.child("name").getValue().toString(),ds.child("phone").getValue().toString());
//          //      Customer customer = ds.getValue(Customer.class);
//            Log.d("save","keys "+ ds.getValue(Customer.class).getName() );
//            //customers.add(customer);
//        }
//
//    }
     // Retrive data

    public ArrayList<Customer> Retrive(){

        db.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                   // Log.d("data","objects" + dataSnapshot.getChildrenCount());
                   // FetchData(dataSnapshot);

                Customer customer = dataSnapshot.getValue(Customer.class);
                customers.add(customer);
                Log.d("save","data "+customers.size());
                //not

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                   // FetchData(dataSnapshot);
                Customer customer = dataSnapshot.getValue(Customer.class);
                Log.d("save",customer.toString());
                customers.add(customer);

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return customers;
    }

}
