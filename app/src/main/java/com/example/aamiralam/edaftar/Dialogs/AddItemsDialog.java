package com.example.aamiralam.edaftar.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import com.example.aamiralam.edaftar.Models.Item;
import com.example.aamiralam.edaftar.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class AddItemsDialog  extends AppCompatDialogFragment{
    private AutoCompleteTextView editTextItemName;
    private EditText editTextItemAmount;
    private ArrayList<Item> items = new ArrayList<>();

    private Item selected_item;
    private String total_amount;

    // Interface and  listener to this add customer dialog

    public interface AddItemsListener {
        void applyText(Item item, String item_amount);
    }

    AddItemsListener addItemsListener;

    private static final String[] ITEMS = new String[]{
            "Bread" , "Brown Bread", "Eggs", "Wheat", "Daal", "Juice","Beads"
    };



    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder  builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.add_items_dialog, null);

        builder.setView(view)
                .setTitle("Add Item")
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //nothing happend on cancel
                    }
                })
                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String name = editTextItemName.getText().toString();
                        String amount = editTextItemAmount.getText().toString();


                        addItemsListener.applyText(selected_item, amount);

                    }
                });

        editTextItemName = view.findViewById(R.id.editTextItemName);
        editTextItemAmount = view.findViewById(R.id.editTextItemAmount);
        // loading data from json object
        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset());
            JSONArray m_jArry = obj.getJSONArray("data");


            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                Log.d("Details-->", jo_inside.getString("name"));
                String item_name = jo_inside.getString("name");
                String item_price = jo_inside.getString("price");
                String item_id = jo_inside.getString("id");
                Item item = new Item(item_name, item_price, item_id);
                items.add(item);


            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

       AutoCompleteItemsAdapter autoCompleteItemsAdapter = new AutoCompleteItemsAdapter(getContext(),R.layout.items_name_auto_complete, items);

       // ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),R.layout.items_name_auto_complete, R.id.textView_auto_complete_list_items,ITEMS);
        editTextItemName.setAdapter(autoCompleteItemsAdapter);
        editTextItemName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Item item = (Item) parent.getItemAtPosition(position);

                editTextItemName.setText(item.getName() + " Rs. "+item.getPrice()+"");
                selected_item = item;


            }
        });


        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            addItemsListener = (AddItemsListener) context;

        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()+ "must implement example dialog listener" + e.getMessage());
        }
    }
  //load local json data from assets folder
    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getActivity().getAssets().open("inventory_items.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            Log.d("json","Error while reading json");
            return null;
        }
        return json;
    }



}
