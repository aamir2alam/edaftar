package com.example.aamiralam.edaftar;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aamiralam.edaftar.Models.Customer;
import com.example.aamiralam.edaftar.Models.ShoppingCart;
import com.example.aamiralam.edaftar.Models.Transaction;
import com.example.aamiralam.edaftar.ViewHolder.CustomerTransactionsViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Calendar;
import java.util.List;

import static com.firebase.ui.auth.AuthUI.getApplicationContext;
import static com.itextpdf.text.Font.BOLD;

public class DailySalesFragment extends Fragment {


    private int STORAGE_PERMISSION_CODE = 1;
    TextView textViewSales, textViewPayments, textViewPending, textViewItemsSold, textViewTransactions, textViewNewCustomers;
    private FloatingActionButton floatingActionButton_save;
    FirebaseAuth mAuth;
    DatabaseReference venderRef;
    ListView listView;
    TransactionsAdapter transactionsAdapter;
    private ArrayList<Transaction> transactions = new ArrayList<>();
    private int CustomerTotalTransactions, CustomerTotalPurchase , CustomerTotalPayments , ItemsSold, NewCustomers;
    private ProgressDialog progress;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_daily_sales , container, false);

        Toolbar toolbar =  view.findViewById(R.id.toolbar);
        toolbar.setTitle( getToday() );

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);

        // initializing views
        listView = view.findViewById(R.id.transaction_list);
        textViewSales = view.findViewById(R.id.sales);
        textViewPayments = view.findViewById(R.id.today_payments);
        textViewPending = view.findViewById(R.id.today_pending);
        textViewItemsSold = view.findViewById(R.id.today_items_sold);
        textViewTransactions = view.findViewById(R.id.todays_transactions);
        textViewNewCustomers = view.findViewById(R.id.todays_new_customers);
        floatingActionButton_save = view.findViewById(R.id.save_to_pdf);

        transactionsAdapter = new TransactionsAdapter(getContext(), R.layout.transactions_list_layout,transactions);
        listView.setAdapter(transactionsAdapter);
        Log.d("date",getStartOfDay() + "  "+ getEndOfDay());
      //  Home home = (Home)getActivity();

        mAuth = FirebaseAuth.getInstance();
        venderRef = FirebaseDatabase.getInstance().getReference("registered_vendors").child(mAuth.getUid()).child("customers");


        NewCustomers = 0;
        venderRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot customerSnapshot: dataSnapshot.getChildren()){
                    Customer customer = customerSnapshot.getValue(Customer.class);

                   getTransactions( customer.get_id() );
                   if( customer.getDate()  > getStartOfDay()){
                       NewCustomers ++;
                       textViewNewCustomers.setText( String.valueOf( NewCustomers ));
                   }


                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                    showWithItems(view , transactions.get(position));



            }
        });

        floatingActionButton_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ContextCompat.checkSelfPermission(getContext(),
                        Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){

                    createandDisplayPdf();

                }else{
                    requestStoragePermission();
                }

            }
        });


        return view;
    }

    private void requestStoragePermission() {
        if(ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE)){
                new AlertDialog.Builder(getContext())
                           .setTitle("Permission needed")
                            .setMessage("This permission is needed to save daily sales record into pdf")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(getActivity(),
                                        new String[] {Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create().show();

        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[] {Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == STORAGE_PERMISSION_CODE)  {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getContext(), "Permission GRANTED", Toast.LENGTH_SHORT).show();
                createandDisplayPdf();
            } else {
                Toast.makeText(getContext(), "Permission DENIED", Toast.LENGTH_SHORT).show();
            }
        }
    }


    public int getItemsCount(ArrayList<ShoppingCart> shoppingCarts) {
        int count =0 ;
        for(int i=0; i< shoppingCarts.size();i++ ){
            count = count + Integer.parseInt( shoppingCarts.get(i).getNumber_of_items() );
        }
        return count;
    }
    public synchronized void  getTransactions(String cust_id){

        CustomerTotalPurchase = 0;
        CustomerTotalPayments = 0;
        ItemsSold = 0;
        CustomerTotalTransactions = 0;

        Query mDatabase = FirebaseDatabase.getInstance().getReference("cust_transactions").child(mAuth.getUid()).child(cust_id).child("transactions")
                                .orderByChild("date").startAt(getStartOfDay()).endAt(getEndOfDay());

                mDatabase.addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                        Transaction transaction =dataSnapshot.getValue(Transaction.class);
                            transactions.add(transaction);
                            transactionsAdapter.notifyDataSetChanged();
                        if(transaction.getType().equals("purchase")){
                            CustomerTotalPurchase   = CustomerTotalPurchase +Integer.parseInt(transaction.getTotal_amount());

                            ItemsSold = ItemsSold + getItemsCount(transaction.getItems());

                        }else{

                            CustomerTotalPayments = CustomerTotalPayments + Integer.parseInt(transaction.getTotal_amount());
                        }
                        CustomerTotalTransactions++;
                        Log.d("today",transaction.getDate()+ " "+ transaction.getTotal_amount() );
                        Log.d("today","todays transactions "+ CustomerTotalTransactions);
                        textViewTransactions.setText( String.valueOf( CustomerTotalTransactions));
                        textViewSales.setText( String.valueOf( CustomerTotalPurchase));
                        textViewPending.setText( String.valueOf( CustomerTotalPurchase - CustomerTotalPayments));
                        textViewPayments.setText( String.valueOf( CustomerTotalPayments) );
                        textViewItemsSold.setText( String.valueOf( ItemsSold));

                    }

                    @Override
                    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });



    }


    public void showWithItems(View view, Transaction transaction) {

        if(transaction.getType().equals("payment"))
                return;

        final CharSequence[] items = new CharSequence[transaction.getItems().size()];
        int i= 0;
        for(ShoppingCart shoppingCart: transaction.getItems() ){

            items[i] =  shoppingCart.getCart_item().getName() + " "+shoppingCart.getCart_item().getPrice()+" SR"+ "     "+shoppingCart.getNumber_of_items()+ " Units";
            i++;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Cart items");
        builder.setItems(items, null);


        builder.setPositiveButton("OK", null);

        builder.setIcon(getResources().getDrawable(R.drawable.ic_shopping_basket_primary_24dp));

        AlertDialog alertDialog = builder.create();
        Log.d("alert", transaction.getType()+ " "+ view.toString());
        alertDialog.show();

        Button button = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        button.setPadding(0, 0, 20, 0);
        //button.setTextColor(Color.WHITE);
    }


    //creating toolbar buttons and their actions
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.toolbar_menu_others, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.settings_item:
                Intent settingsIntent = new Intent(getContext(), SettingsActivity.class);
                getContext().startActivity(settingsIntent);
                Toast.makeText(getContext(), "Settings called", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.logout_item:
                final FirebaseAuth mAuth = FirebaseAuth.getInstance();
                mAuth.signOut();
                Intent myIntent = new Intent(getContext(), MainActivity.class);
                getContext().startActivity(myIntent);
                getActivity().finish();
                return true;
            case R.id.search:
                Toast.makeText(getContext(), "Search called", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }



    }

    public long getStartOfDay() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DATE);
        calendar.set(year, month, day, 0, 0, 0);

        return calendar.getTime().getTime();
    }

    private long getEndOfDay() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DATE);

        calendar.set(year, month, day, 23, 59, 59);

        return calendar.getTime().getTime();
    }

    private String getToday() {
        Calendar  calendar = Calendar.getInstance();

        SimpleDateFormat dayFormater = new SimpleDateFormat("EEEE");
        String day = dayFormater.format(calendar.getTime());
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        return  day+"  "+ df.format(calendar.getTime());
    }

    private String getFormatedDateString(long time_stamp) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        java.sql.Date netDate = (new java.sql.Date(time_stamp));
        return   sdf.format(netDate);
    }

    // Adapter to render list of transactions
    public class TransactionsAdapter extends ArrayAdapter<Transaction> {



        public TransactionsAdapter(@NonNull Context context, int resource, @NonNull List<Transaction> objects) {
            super(context, resource, objects);
        }

        @Override
        public int getCount() {
            return super.getCount();
        }

        @Nullable
        @Override
        public Transaction getItem(int position) {
            return super.getItem(position);
        }

        @Override
        public long getItemId(int position) {
            return super.getItemId(position);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View view, @NonNull ViewGroup viewGroup) {
            // return super.getView(position, view, viewGroup);
            view =  getLayoutInflater().inflate(R.layout.transactions_list_layout,null);
            TextView textViewItems = view.findViewById(R.id.textViewItems);
            TextView textViewDate = view.findViewById(R.id.textViewDate);
            TextView textViewTotal = view.findViewById(R.id.textViewTotal);
            ImageView imageView = view.findViewById(R.id.transaction_icon);
            final Transaction transaction = getItem(position);

            textViewDate.setText( getFormatedDateString( transaction.getDate() ));

          //  Log.d("type",transaction.getType() + transactions.get(position).getType());
            if(transactions.get(position).getType().equals("purchase")){
                textViewItems.setText(transaction.getItems().size() + " Items");
                textViewTotal.setText("- " +transaction.getTotal_amount());
                textViewTotal.setTextColor(getResources().getColor(R.color.primaryRed));
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_shopping_basket_primary_24dp));
            }else{
                textViewItems.setText("Payment");
                textViewTotal.setText("+ " +transaction.getTotal_amount());
                textViewTotal.setTextColor(getResources().getColor(R.color.green));
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_payment_gray_24dp));
            }



            return view;
        }
    }

    // Method for creating a pdf file from text, saving it then opening it for display
    public void createandDisplayPdf() {

        if(transactions == null  && transactions.size() ==0 ){
            Toast.makeText(getContext(),"Nothing to print",Toast.LENGTH_SHORT).show();
            return;
        }

        progress = new ProgressDialog(getContext());
        progress.setMessage("Please wait..");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setCanceledOnTouchOutside(false);
        progress.show();

        String file_name = getEndOfDay()+".pdf";
        Document doc = new Document();
        doc.setPageSize(PageSize.A4);
        // fonts used in document
        BaseColor mColorAccent = new BaseColor(0, 153, 204, 255);
        float mHeadingFontSize = 20.0f;
        float mValueFontSize = 26.0f;

        // LINE SEPARATOR
        LineSeparator lineSeparator = new LineSeparator();
        lineSeparator.setLineColor(new BaseColor(0, 0, 0, 68));

        // path where file is being stored
        String path = Environment.getExternalStorageDirectory().getAbsolutePath()+"/Edaftar/Daily Sales" ;
        try {

            File dir = new File(path);
            if(!dir.exists())
                dir.mkdirs();

            Log.d("file",file_name);

            File file = new File(dir, file_name);
            FileOutputStream fOut = new FileOutputStream(file);

            try {
                PdfWriter.getInstance(doc, fOut);
            } catch (DocumentException e) {
                e.printStackTrace();
            }

            //open the document
            doc.open();
            // Adding Title....
            Font mOrderDetailsTitleFont = new Font(Font.FontFamily.COURIER, 36.0f, Font.NORMAL, BaseColor.BLACK);
            Chunk mOrderDetailsTitleChunk = new Chunk("Todays Transactions", mOrderDetailsTitleFont);
            // Creating Paragraph to add...
            Paragraph mOrderDetailsTitleParagraph = new Paragraph(mOrderDetailsTitleChunk);
            // Setting Alignment for Heading
            mOrderDetailsTitleParagraph.setAlignment(Element.ALIGN_CENTER);
            // Finally Adding that Chunk
            doc.add(mOrderDetailsTitleParagraph);
            Paragraph p1 = new Paragraph(getToday());
            Font paraFont= new Font(Font.FontFamily.COURIER);
            p1.setAlignment(Paragraph.ALIGN_CENTER);
            p1.setFont(paraFont);
            //add paragraph to document
            doc.add(p1);
            //Add line saperator
            doc.add(new Paragraph(""));
            doc.add(new Paragraph(""));
            doc.add(new Chunk(lineSeparator));
            doc.add(new Paragraph(""));
            doc.add(new Paragraph(""));
            doc.add(new Paragraph(""));


            doc.add(createPdfTable());

            doc.add(new Paragraph(""));
            doc.add(new Paragraph(""));
            doc.add(new Paragraph(""));

            PdfPTable table = new PdfPTable(2);
            table.setWidths(new int[]{3, 1});

            table.addCell(new PdfPCell(new Phrase("Total Sales") )).setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(new PdfPCell(new Phrase(String.valueOf(CustomerTotalPurchase)+" SR"))).setHorizontalAlignment(Element.ALIGN_CENTER);

            table.addCell(new PdfPCell(new Phrase("Received Amount"))).setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(new PdfPCell(new Phrase(String.valueOf(CustomerTotalPayments)+" SR"))).setHorizontalAlignment(Element.ALIGN_CENTER);

            table.addCell(new PdfPCell(new Phrase("Pending Amount"))).setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(new PdfPCell(new Phrase(String.valueOf(CustomerTotalPurchase - CustomerTotalPayments)+" SR"))).setHorizontalAlignment(Element.ALIGN_CENTER);
            doc.add(table);

            doc.add(new Paragraph(""));
            doc.add(new Paragraph(""));
            doc.add(new Chunk(lineSeparator));
            doc.add(new Paragraph(""));
            doc.add(new Paragraph(""));




        } catch (DocumentException de) {
            Log.e("PDFCreator", "DocumentException:" + de);
        } catch (IOException e) {
            Log.e("PDFCreator", "ioException:" + e);
        }
        finally {
            doc.close();
        }

        progress.dismiss();

        viewPdf(file_name, path);
    }


    public  PdfPTable createPdfTable()
            throws IOException, DocumentException {


        PdfPTable table = new PdfPTable(4);
        table.setWidths(new int[]{1, 2, 1, 1});

        table.addCell(new PdfPCell(new Phrase("Type") )).setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(new PdfPCell(new Phrase("Date"))).setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(new PdfPCell(new Phrase("Items"))).setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(new PdfPCell(new Phrase("Total"))).setHorizontalAlignment(Element.ALIGN_CENTER);

        PdfPCell[] cells = table.getRow(0).getCells();
        for (int j=0;j<cells.length;j++){
            cells[j].setBackgroundColor(BaseColor.GRAY);
        }

        for (Transaction transaction : transactions) {
             table.addCell(new PdfPCell(new Phrase( transaction.getType() ))).setHorizontalAlignment(Element.ALIGN_CENTER);
             table.addCell(new PdfPCell(new Phrase( getFormatedDateString( transaction.getDate() )))).setHorizontalAlignment(Element.ALIGN_CENTER);
             if( transaction.getType().equals("purchase")){
                 table.addCell(new PdfPCell(new Phrase( String.valueOf( transaction.getItems().size()+" Items" )))).setHorizontalAlignment(Element.ALIGN_CENTER);
             }else{
                 table.addCell(new PdfPCell(new Phrase(  "Cash"))).setHorizontalAlignment(Element.ALIGN_CENTER);
             }
             table.addCell(new PdfPCell(new Phrase( transaction.getTotal_amount()+" SR"))).setHorizontalAlignment(Element.ALIGN_CENTER);
        }

        return table;
    }

    // Method for opening a pdf file
    private void viewPdf(String file_name, String directory) {


        File file = new File(directory +"/"+ file_name);
        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setDataAndType(Uri.fromFile(file),"application/pdf");
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

        Intent intent = Intent.createChooser(target, "Open File");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            // Instruct the user to install a PDF reader here, or something
            Toast.makeText(getContext(), "Please install a pdf reader to view", Toast.LENGTH_SHORT).show();
        }

    }


}
