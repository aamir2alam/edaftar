//package com.example.aamiralam.edaftar.Adapters;
//
//import android.content.Context;
//import android.content.Intent;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ArrayAdapter;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.example.aamiralam.edaftar.CustomerThreadActivity;
//
//
//import com.example.aamiralam.edaftar.Models.Customer;
//import com.example.aamiralam.edaftar.R;
//
//import java.util.ArrayList;
//
//
//public class CustomAdapterCustomers extends ArrayAdapter<Customer> {
//     Context context;
//     ArrayList<Customer> customers;
//
//
//    public CustomAdapterCustomers(@NonNull Context context, int resource , @NonNull ArrayList<Customer> objects) {
//        super(context, resource, objects);
//        this.context = context;
//        this.customers = objects;
//
//    }
//
//    @Override
//    public int getCount() {
//        return customers.size();
//    }
//
//    @Nullable
//    @Override
//    public Customer getItem(int position) {
//        return super.getItem(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return super.getItemId(position);
//    }
//
//    @NonNull
//    @Override
//    public View getView(int position, @Nullable View view, @NonNull ViewGroup parent) {
//        // return super.getView(position, view, viewGroup);
//
//            view  = LayoutInflater.from(context).inflate(R.layout.customer_layout,null);
//
//
//
//        TextView textView_name = view.findViewById(R.id.textView_name);
//        TextView textView_phone = view.findViewById(R.id.textView_phone);
//
//        final Customer customer = this.getItem(position);
//
//        textView_name.setText(customer.getName());
//        textView_phone.setText(customer.getPhone());
//
//        notifyDataSetChanged();
//
//        view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(getContext(),customer.getName(),Toast.LENGTH_SHORT).show();
//
//
//
//                Intent customerIntent = new Intent(v.getContext(), CustomerThreadActivity.class);
//                v.getContext().startActivity(customerIntent);
//            }
//        });
//
//        return view;
//    }
//
//
//
//
//
//}
