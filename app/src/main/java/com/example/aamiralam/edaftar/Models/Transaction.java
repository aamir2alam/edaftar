package com.example.aamiralam.edaftar.Models;

import java.util.ArrayList;

public class Transaction {

    private String type;
    private ArrayList<ShoppingCart> items;
    private String total_amount;
    // date is time_stamp formate convert it into date string before rendering
    private long date;

    public Transaction(){

    }


    public Transaction(String type, ArrayList<ShoppingCart> items, String total_amount, long date) {
        this.type = type;
        this.items = items;
        this.total_amount = total_amount;
        this.date = date;
    }

    public Transaction(String type , String total_amount, long date){
        this.type = type;
        this.total_amount = total_amount;
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<ShoppingCart> getItems() {
        return items;
    }

    public void setItems(ArrayList<ShoppingCart> items) {
        this.items = items;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }
}
