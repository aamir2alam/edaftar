package com.example.aamiralam.edaftar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v7.widget.Toolbar;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.aamiralam.edaftar.Dialogs.AddCustomersDialog;
import com.example.aamiralam.edaftar.Models.Customer;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class HomeFragment extends Fragment implements AddCustomersDialog.AddCustomersListner {

    private FloatingActionButton floatingActionButton_add;
    MaterialSearchView materialSearchView;

    //HomeFragmentListener homeFragmentListener;
    public Context context;
    private CustomersCustomAdapter customersCustomAdapter;

    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    ListView listView;

    public ArrayList<Customer> customers = new ArrayList<>();

    // create an interface of home_fragment in order to pass data to other activity or fragment
//    public interface HomeFragmentListener {
//         void onDataSent(Customer customer);
//    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = getView() != null ? getView() : inflater.inflate(R.layout.fragment_home , container, false);

        //initialize list view with custom adapter
        listView =  (ListView) view.findViewById(R.id.list_view);

        //setup toolbar
        Toolbar toolbar =  view.findViewById(R.id.toolbar);
        toolbar.setTitle("Customers");
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);

        //Adapter
        customersCustomAdapter = new CustomersCustomAdapter(getContext(),R.layout.customer_layout, customers );
        listView.setAdapter(customersCustomAdapter);

        //setup firebase configs to use in this fragment
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference("registered_vendors").child(mAuth.getUid()).child("customers");

        mDatabase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Customer customer = dataSnapshot.getValue(Customer.class);
                Log.d("save",customer.get_id() + customer.getName());
                customers.add(customer);
                customersCustomAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               // homeFragmentListener.onDataSent(customers.get(position));
                Log.d("cust","calling method" );
                // get the reference to its parent activity
                Home home = (Home)getActivity();
                home.startNewActivity(customers.get(position));

            }
        });


        // add action listener to floating action button
        floatingActionButton_add = view.findViewById(R.id.add_customers_button);
        floatingActionButton_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }
        });

        //add material search view
//         materialSearchView =  view.findViewById(R.id.mat_searchView);
//        materialSearchView.closeSearch();
//        materialSearchView
//        materialSearchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String newText) {
//
//              //  Toast.makeText(getContext(),newText,Toast.LENGTH_SHORT).show();
//                return false;
//            }
//        });

        //add list item click listener

//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                //send data to the customer thread activity
//                homeFragmentListener.onDataSent(CustomerList.get(position));
//
//                Intent intent = new Intent(getContext(), CustomerThreadActivity.class);
//                getContext().startActivity(intent);
//                Toast.makeText(getContext(),CustomerList.get(position).getName(),Toast.LENGTH_SHORT).show();
//            }
//        });


        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    //saving the state of the fragment

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void openDialog() {

        AddCustomersDialog addCustomersDialog = new AddCustomersDialog();
        addCustomersDialog.setTargetFragment(HomeFragment.this,1);
        addCustomersDialog.show(getFragmentManager(),"Add dialog");

    }

    // override method from interface AddCustomersLiatener to get the data from the dialog b
       @Override
    public void applyText(String customer_name, String phone_number) {

        // here we get out data from add cuatomer dialog
        if(customer_name == null || customer_name.equals("") || customer_name.length() < 2){
            Toast.makeText(getContext(),"Invalid customer name",Toast.LENGTH_SHORT).show();
            return;
        }
           Calendar calendar = Calendar.getInstance();
           String puch_key = mDatabase.push().getKey();
           final long time_stamp = calendar.getTime().getTime();
           Customer customer  = new Customer(customer_name, phone_number, puch_key, time_stamp);

        try {
               mDatabase.push().setValue(customer);
           } catch (Exception e) {
               e.printStackTrace();
           }

           Toast.makeText(getContext(),"Customer added",Toast.LENGTH_SHORT).show();

    }


    // creating toolbar buttons and their actions

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.toolbar_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.settings_item:
                Intent settingsIntent = new Intent(getContext(), SettingsActivity.class);
                getContext().startActivity(settingsIntent);
                return true;
            case R.id.logout_item:
                mAuth.signOut();
                Intent myIntent = new Intent(getContext(), MainActivity.class);
                getContext().startActivity(myIntent);
                getActivity().finish();
                return true;
           // case R.id.action_search:
//                materialSearchView.setMenuItem(item);
//                Toast.makeText(getContext(), "Search called", Toast.LENGTH_SHORT).show();
             //   return true;
            default:
                 return super.onOptionsItemSelected(item);
        }
    }

    // creating list of customers using custom adapter
    public class CustomersCustomAdapter extends ArrayAdapter<Customer> {

        public CustomersCustomAdapter(@NonNull Context context, int resource, @NonNull List<Customer> objects) {
            super(context, resource, objects);
        }

        @Override
        public int getCount() {
            return super.getCount();
        }

        @Nullable
        @Override
        public Customer getItem(int position) {
            return super.getItem(position);
        }

        @Override
        public long getItemId(int position) {
            return super.getItemId(position);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View view, @NonNull ViewGroup viewGroup) {

            view =  getLayoutInflater().inflate(R.layout.customer_layout,null);
            TextView textView_name = view.findViewById(R.id.textView_name);
            TextView textView_phone = view.findViewById(R.id.textView_phone);
            final Customer customer = this.getItem(position);

            textView_name.setText(customer.getName());
            textView_phone.setText(customer.getPhone());

//            view.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    homeFragmentListener.onDataSent(customer);
//                    Log.d("cust","calling");
//
//                Intent intent = new Intent(getContext(), CustomerThreadActivity.class);
//                getContext().startActivity(intent);
//                }
//
//            });

            return view;
        }
    }


    // codes for fragment listener
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if( context instanceof HomeFragmentListener){
//            Log.d("cust","true");
//            homeFragmentListener = (HomeFragmentListener) context ;
//        }else{
//            throw new RuntimeException(context.toString() + "Must implement home fragment expetion");
//        }
//   }
//
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        homeFragmentListener = null;
//    }
}
