package com.example.aamiralam.edaftar.Models;

import android.os.Parcel;
import android.os.Parcelable;

public class ShoppingCart  implements Parcelable{


    private Item cart_item;
    private String number_of_items;
    private String total_price;

    public ShoppingCart(Item cart_item, String number_of_items, String total_price) {
        this.cart_item = cart_item;
        this.number_of_items = number_of_items;
        this.total_price = total_price;
    }

    public ShoppingCart(){

    }

    protected ShoppingCart(Parcel in) {
        number_of_items = in.readString();
        total_price = in.readString();
    }

    public static final Creator<ShoppingCart> CREATOR = new Creator<ShoppingCart>() {
        @Override
        public ShoppingCart createFromParcel(Parcel in) {
            return new ShoppingCart(in);
        }

        @Override
        public ShoppingCart[] newArray(int size) {
            return new ShoppingCart[size];
        }
    };

    public Item getCart_item() {
        return cart_item;
    }

    public void setCart_item(Item cart_item) {
        this.cart_item = cart_item;
    }

    public String getNumber_of_items() {
        return number_of_items;
    }

    public void setNumber_of_items(String number_of_items) {
        this.number_of_items = number_of_items;
    }

    public String getTotal_price() {
        return total_price;
    }

    public void setTotal_price(String total_price) {
        this.total_price = total_price;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(number_of_items);
        dest.writeString(total_price);
    }
}
