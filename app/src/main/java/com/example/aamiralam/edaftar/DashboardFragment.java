package com.example.aamiralam.edaftar;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aamiralam.edaftar.Models.ShoppingCart;
import com.example.aamiralam.edaftar.Models.Transaction;
import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;


import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class DashboardFragment extends Fragment {

    private CombinedChart mChartSales, mChartPayments;
    private TextView textViewSales, textViewPendingAmount, textViewRecievedAmount ,
            textViewItemsSold, textViewCustomers;
    FirebaseAuth mAuth;
    DatabaseReference mDatabase, venderRef;



    private int CustomerTotalPurchase, CustomerTotalPayments, CustomerTotalTransactions, ItemsSold, TotalCustomers;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dashboard , container, false);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference("cust_transactions").child(mAuth.getUid());

        venderRef = FirebaseDatabase.getInstance().getReference("registered_vendors").child(mAuth.getUid()).child("customers");

        textViewSales = v.findViewById(R.id.total_sales_data);
        textViewCustomers = v.findViewById(R.id.customers_data);
        textViewPendingAmount = v.findViewById(R.id.pending_amount_data);
        textViewRecievedAmount = v.findViewById(R.id.recieved_amount_data);
        textViewItemsSold = v.findViewById(R.id.item_sold_data);

        Toolbar toolbar =  v.findViewById(R.id.toolbar);
        toolbar.setTitle("Aamir Alam   Shop: CD5214");


        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);
        String i =   getEndOfDay();
        //initializing CombinedChart
        mChartSales = (CombinedChart) v.findViewById(R.id.chart1);
        // draw bars behind lines
        mChartSales.setDrawOrder(new CombinedChart.DrawOrder[]{
                CombinedChart.DrawOrder.BAR,  CombinedChart.DrawOrder.LINE
        });

        //Set the Legends Orientation
        Legend l = mChartSales.getLegend();
        l.setWordWrapEnabled(true);
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        //Set Right And Left Axis
        YAxis rightAxis = mChartSales.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        YAxis leftAxis = mChartSales.getAxisLeft();
        leftAxis.setDrawGridLines(false);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        //Set X-Axis Lables
        XAxis xAxis = mChartSales.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTH_SIDED);
        xAxis.setAxisMinimum(0f);
        xAxis.setGranularity(1f);
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return mMonths[(int) value % mMonths.length];
            }
        });

        //Create CombinedData Object
        CombinedData data = new CombinedData();

       // data.setData( generateLineData());
        data.setData(generateBarData());

        //Finally Create Chart
        xAxis.setAxisMaximum(data.getXMax() + 0.25f);
        mChartSales.setData(data);
        mChartSales.animate();
        mChartSales.invalidate();

        TotalCustomers = 0;
        venderRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot customerSnapshot: dataSnapshot.getChildren()){

                    calculateTotal(customerSnapshot.child("_id").getValue().toString());
                    TotalCustomers++;
                    Log.d("transactions","customers "+TotalCustomers);
                    textViewCustomers.setText(String.valueOf(TotalCustomers));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



        return v;
    }
    public int getItemsCount(ArrayList<ShoppingCart> shoppingCarts) {
                int count =0 ;
          for(int i=0; i< shoppingCarts.size();i++ ){
                count = count + Integer.parseInt( shoppingCarts.get(i).getNumber_of_items() );
          }
        return count;
    }
    public synchronized void  calculateTotal(String cust_id){

         CustomerTotalPurchase = 0;
         CustomerTotalPayments = 0;
         CustomerTotalTransactions = 0;
         ItemsSold = 0;
         //2018-12-11 00:00  2018-12-11 23:59
//        Query query = FirebaseDatabase.getInstance().getReference("cust_transactions").child(mAuth.getUid())
//                                .child(cust_id).child("trabsactions").orderByChild("date").startAt("2018-12-11 00:00").endAt("2018-12-11 23:59");
        mDatabase.child(cust_id).child("transactions")
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                        Transaction transaction =dataSnapshot.getValue(Transaction.class);

                        if(transaction.getType().equals("purchase")){
                            CustomerTotalPurchase   = CustomerTotalPurchase +Integer.parseInt(transaction.getTotal_amount());

                            ItemsSold = ItemsSold + getItemsCount(transaction.getItems());

                        }else{

                            CustomerTotalPayments = CustomerTotalPayments + Integer.parseInt(transaction.getTotal_amount());
                        }
                        CustomerTotalTransactions++;
                         Log.d("today",transaction.getDate()+ " "+ transaction.getTotal_amount() );
                         Log.d("today","todays transactions "+ CustomerTotalTransactions);
                        textViewSales.setText(String.valueOf(CustomerTotalPurchase));
                        textViewPendingAmount.setText(String.valueOf(CustomerTotalPurchase - CustomerTotalPayments));
                        textViewRecievedAmount.setText( String.valueOf(CustomerTotalPayments) );
                        textViewItemsSold.setText(String.valueOf(ItemsSold));

                    }

                    @Override
                    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });



    }

//    //creating tooolbar buttons and their actions
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.toolbar_menu_others, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.settings_item:
                Intent settingsIntent = new Intent(getContext(), SettingsActivity.class);
                getContext().startActivity(settingsIntent);
                return true;
            case R.id.logout_item:
                final FirebaseAuth mAuth = FirebaseAuth.getInstance();
                mAuth.signOut();
                Intent loginIntent = new Intent(getContext(), MainActivity.class);
                getContext().startActivity(loginIntent);
                getActivity().finish();
                return true;
            case R.id.search:
                Toast.makeText(getContext(), "Search called", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    protected String[] mMonths = new String[] {
            "Jan", "Feb", "Mar", "Apr", "May", "June","Jul","Aug","Sep","Oct","Nov","Dec"
    };


    private ArrayList<Entry> getLineEntriesData(ArrayList<Entry> entries){
        entries.add(new Entry(1, 20));
        entries.add(new Entry(2, 10));
        entries.add(new Entry(3, 8));
        entries.add(new Entry(4, 40));
        entries.add(new Entry(5, 37));
        entries.add(new Entry(6,44));
        entries.add(new Entry(7,55));
        entries.add(new Entry(8,43));
        entries.add(new Entry(9,65));
        entries.add(new Entry(10,53));
        entries.add(new Entry(11,22));
        entries.add(new Entry(12,76));

        return entries;
    }

    private ArrayList<BarEntry> getBarEnteries(ArrayList<BarEntry> entries){
        entries.add(new BarEntry(1, 25));
        entries.add(new BarEntry(2, 30));
        entries.add(new BarEntry(3, 38));
        entries.add(new BarEntry(4, 10));
        entries.add(new BarEntry(5, 15));
        entries.add(new BarEntry(6,46));
        entries.add(new BarEntry(7,57));
        entries.add(new BarEntry(8,45));
        entries.add(new BarEntry(9,67));
        entries.add(new BarEntry(10,55));
        entries.add(new BarEntry(11,24));
        entries.add(new BarEntry(12,78));
        return  entries;
    }


    private LineData generateLineData() {

        LineData d = new LineData();

        ArrayList<Entry> entries = new ArrayList<Entry>();

        entries = getLineEntriesData(entries);

        LineDataSet set = new LineDataSet(entries, "Line");
        //set.setColor(Color.rgb(240, 238, 70));
        set.setColors(ColorTemplate.MATERIAL_COLORS);
        set.setLineWidth(2.5f);
        set.setCircleColor(Color.rgb(240, 238, 70));
        set.setCircleRadius(5f);
        set.setFillColor(Color.rgb(240, 238, 70));
        set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        set.setDrawValues(true);
        set.setValueTextSize(10f);
        set.setValueTextColor(Color.rgb(240, 238, 70));

        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        d.addDataSet(set);

        return d;
    }

    private BarData generateBarData() {

        ArrayList<BarEntry> entries = new ArrayList<BarEntry>();
        entries = getBarEnteries(entries);

        BarDataSet set1 = new BarDataSet(entries, "Bar");
        //set1.setColor(Color.rgb(60, 220, 78));
        set1.setColors(new int[] {Color.RED, Color.RED, Color.RED, Color.RED, Color.RED});
        set1.setValueTextColor(Color.GRAY);
        set1.setValueTextSize(10f);
        set1.setAxisDependency(YAxis.AxisDependency.LEFT);

        float barWidth = 0.45f; // x2 dataset


        BarData d = new BarData(set1);
        d.setBarWidth(barWidth);


        return d;
    }

    private String getEndOfDay() {
        Calendar calendar  = Calendar.getInstance();
        Calendar calendar1  = Calendar.getInstance();
        Log.d("date","instance "+calendar);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DATE);

        calendar.set(year, month, day, 23, 59, 59);

        long time =  calendar.getTime().getTime();
        long time2 = calendar1.getTime().getTime();


        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String formatted_date_string = df.format(calendar.getTime());


//        convert unix epoch timestamp (seconds) to milliseconds


        try{

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            Date netDate = (new Date(time));

             Log.d("date","new date  "+ sdf.format(netDate));
        }
        catch(Exception ex){
            Log.d("expetion",ex.toString());
        }

        return formatted_date_string;
    }


}
