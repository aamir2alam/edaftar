package com.example.aamiralam.edaftar;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aamiralam.edaftar.Models.Customer;
import com.example.aamiralam.edaftar.Models.ShoppingCart;
import com.example.aamiralam.edaftar.Models.Transaction;
import com.example.aamiralam.edaftar.ViewHolder.CustomerTransactionsViewHolder;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class CustomerThreadActivity extends AppCompatActivity  {

     TextView name , phone, total_purchase, total_transactions;
     Button addItem, addMoney;
     RecyclerView recyclerView;
     LinearLayoutManager linearLayoutManager;
   // TransactionsAdapter transactionsAdapter;
    // initialize adapter option and adapter
    FirebaseRecyclerOptions<Transaction> options;
    FirebaseRecyclerAdapter<Transaction, CustomerTransactionsViewHolder> adapter;
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

   private int CustomerTotalTransactions = 0, CustomerTotalPurchase = 0, CustomerTotalPayments = 0;
   private int PendingAmount = 0;

    // private ArrayList<Transaction> transactions = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_thread);

        name = findViewById(R.id.text_name);
        phone = findViewById(R.id.text_phone);
        addItem = findViewById(R.id.add_items);
        addMoney = findViewById(R.id.add_money);
        //listView = findViewById(R.id.transaction_list);
        recyclerView = findViewById(R.id.transaction_list);
        total_purchase = findViewById(R.id.transaction_payment);
        total_transactions = findViewById(R.id.total_transactions);

        Intent intent = getIntent();
        final Customer customer = (Customer)intent.getSerializableExtra("customer");
        name.setText(customer.getName());
        phone.setText(customer.getPhone());


        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference("cust_transactions").child(mAuth.getUid()).child(customer.get_id()).child("transactions");
        linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);

        getCurrentTotals(mDatabase);



        recyclerView.setLayoutManager(linearLayoutManager);
       // transactionsAdapter = new TransactionsAdapter(this, R.layout.transactions_list_layout,transactions);
        //listView.setAdapter(transactionsAdapter);
        //setting up the options and adapter configurations
        options = new FirebaseRecyclerOptions.Builder<Transaction>()
                            .setQuery(mDatabase, Transaction.class).build();

        adapter = new FirebaseRecyclerAdapter<Transaction, CustomerTransactionsViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull CustomerTransactionsViewHolder holder, int position, @NonNull final Transaction model) {
                // here we set values from model to the list view elements which is defined inside our view holder
                Log.d("adapter",model.getType());
                if(model.getType().equals("purchase")){
                holder.textViewItems.setText(model.getItems().size() + " Items");
                holder.textViewTotal.setText("- " +model.getTotal_amount());
                holder.textViewTotal.setTextColor(getResources().getColor(R.color.primaryRed));
                holder.textViewDate.setText( getFormatedDateString( model.getDate() ) );
                holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_shopping_basket_primary_24dp));

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d("click","clicked"+ model.getType());
                        showWithItems(v, model);
                    }
                });

            }else{
                holder.textViewItems.setText("Payment");
                holder.textViewTotal.setText("+ " +model.getTotal_amount());
                holder.textViewTotal.setTextColor(getResources().getColor(R.color.green));
                holder.textViewDate.setText( getFormatedDateString( model.getDate() ) );
                holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_payment_gray_24dp));
            }
            }

            @NonNull
            @Override
            public CustomerTransactionsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view  = LayoutInflater.from(parent.getContext()).inflate(R.layout.transactions_list_layout, parent,false);

                return  new CustomerTransactionsViewHolder(view);
            }
        };

        adapter.startListening();
        recyclerView.setAdapter(adapter);

        addItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              Intent addItemIntent = new Intent(CustomerThreadActivity.this, AddItemsActivity.class);
              addItemIntent.putExtra("customer",customer);
              startActivityForResult(addItemIntent,1);
            }
        });

        addMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addMoneyIntent = new Intent(CustomerThreadActivity.this,AddMoneyActivity.class);
                addMoneyIntent.putExtra("current_total",String.valueOf(PendingAmount));
                addMoneyIntent.putExtra("customer",customer);
                startActivityForResult(addMoneyIntent , 2);
            }
        });


    }

    // @desc show transactions detail diaog

    public void showWithItems(View view, Transaction transaction) {

        if(transaction.getType().equals("payment"))
            return;

        final CharSequence[] items = new CharSequence[transaction.getItems().size()];
        int i= 0;
        for(ShoppingCart shoppingCart: transaction.getItems() ){

            items[i] =  shoppingCart.getCart_item().getName() + " "+shoppingCart.getCart_item().getPrice()+" SR"+ "     "+shoppingCart.getNumber_of_items()+ " Units";
            i++;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Cart items");
        builder.setItems(items, null);

        builder.setPositiveButton("OK", null);

        builder.setIcon(getResources().getDrawable(R.drawable.ic_shopping_basket_primary_24dp));

        AlertDialog alertDialog = builder.create();
        alertDialog.show();

        Button button = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        button.setPadding(0, 0, 20, 0);
        //button.setTextColor(Color.WHITE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(adapter != null) {
            adapter.startListening();
        }
    }

    @Override
    protected void onStop() {
        if(adapter != null){
            adapter.stopListening();
        }
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(adapter != null){
            adapter.startListening();
        }
    }

   public void getCurrentTotals(DatabaseReference databaseReference){
            databaseReference.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                    Transaction transaction =dataSnapshot.getValue(Transaction.class);

                if(transaction.getType().equals("purchase")){
                  CustomerTotalPurchase   = CustomerTotalPurchase +Integer.parseInt(transaction.getTotal_amount());

                }else{

                    CustomerTotalPayments = CustomerTotalPayments + Integer.parseInt(transaction.getTotal_amount());
                }
                CustomerTotalTransactions++;

                    PendingAmount = CustomerTotalPurchase - CustomerTotalPayments;
                    total_purchase.setText(CustomerTotalPurchase+" Purchase "+PendingAmount+ " Pending");
                    total_transactions.setText(CustomerTotalTransactions+" Transactions");
                    //Log.d("data","paymenets "+CustomerTotalPayments);
                }

                @Override
                public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
    }

    public String getFormatedDateString(long time_stamp) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date netDate = (new Date(time_stamp));
        return   sdf.format(netDate);
    }

    //    @Override
//    public void onDataSent(Customer customer) {
//        // recive customer data from home fragment
//        this.customer = customer;
//        Toast.makeText(getApplicationContext(),"name "+ customer.getName(),Toast.LENGTH_SHORT).show();
//        Log.d("customer" , "name "+customer.getName());
//        name.setText(customer.getName());
//        phone.setText(customer.getPhone());
//    }

//    public class TransactionsAdapter extends ArrayAdapter<Transaction> {
//
//
//
//        public TransactionsAdapter(@NonNull Context context, int resource, @NonNull List<Transaction> objects) {
//            super(context, resource, objects);
//        }
//
//        @Override
//        public int getCount() {
//            return super.getCount();
//        }
//
//        @Nullable
//        @Override
//        public Transaction getItem(int position) {
//            return super.getItem(position);
//        }
//
//        @Override
//        public long getItemId(int position) {
//            return super.getItemId(position);
//        }
//
//        @NonNull
//        @Override
//        public View getView(int position, @Nullable View view, @NonNull ViewGroup viewGroup) {
//            // return super.getView(position, view, viewGroup);
//            view =  getLayoutInflater().inflate(R.layout.transactions_list_layout,null);
//            TextView textViewItems = view.findViewById(R.id.textViewItems);
//            TextView textViewDate = view.findViewById(R.id.textViewDate);
//            TextView textViewTotal = view.findViewById(R.id.textViewTotal);
//            ImageView imageView = view.findViewById(R.id.transaction_icon);
//            Transaction transaction = getItem(position);
//
//            textViewDate.setText(transaction.getDate());
//
//          //  Log.d("type",transaction.getType() + transactions.get(position).getType());
//            if(transactions.get(position).getType().equals("purchase")){
//                textViewItems.setText(transaction.getItems().size() + " Items");
//                textViewTotal.setText("- " +transaction.getTotal_amount());
//                textViewTotal.setTextColor(getResources().getColor(R.color.primaryRed));
//                imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_shopping_basket_primary_24dp));
//            }else{
//                textViewItems.setText("Payment");
//                textViewTotal.setText("+ " +transaction.getTotal_amount());
//                textViewTotal.setTextColor(getResources().getColor(R.color.green));
//                imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_payment_gray_24dp));
//            }
//
////            view.setOnClickListener(new View.OnClickListener() {
////                @Override
////                public void onClick(View v) {
////
////                    homeFragmentListener.onDataSent(customer);
////                    Log.d("cust","calling");
////
////                Intent intent = new Intent(getContext(), CustomerThreadActivity.class);
////                getContext().startActivity(intent);
////                }
////
////            });
//
//            return view;
//        }
//    }


}
