package com.example.aamiralam.edaftar.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.example.aamiralam.edaftar.R;


public class AddCustomersDialog extends AppCompatDialogFragment {

    private EditText editTextCustomerName;
    private EditText editTextPhoneNumber;

    // Interface and  listener to this add customer dialog

    public interface AddCustomersListner {
        void applyText(String customer_name, String phone_number);
    }

    AddCustomersListner addCustomersListner;



    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder  builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.add_customers_dialog, null);

        builder.setView(view)
                .setTitle("Add Customer")
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //nothing happend on cancel
                    }
                })
                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                         String name = editTextCustomerName.getText().toString();
                         String phone = editTextPhoneNumber.getText().toString();


                        addCustomersListner.applyText(name, phone);

                    }
                });

        editTextCustomerName = view.findViewById(R.id.customer_name);
        editTextPhoneNumber = view.findViewById(R.id.customer_phone);

        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            addCustomersListner = (AddCustomersListner) getTargetFragment();

        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()+ "must implement example dialog listener" + e.getMessage());
        }

    }


}
